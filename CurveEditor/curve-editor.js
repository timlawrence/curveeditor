 var CurveEditor = function(size,div){            	
	var r = Raphael(div, size, size);	
    r.rect(0, 0, size, size, 0).attr({stroke: "#000"});
    this.r = r;
	this.div = div;	
	this.size = size;
	this.anchors = [];
	this.init(size);
};
            
CurveEditor.prototype = {		
		getPath: function(){
			var fanchors = this.util.flatten(this.anchors.map(function(obj){return obj.getPoints();}));
   			return [["M", 0, this.size],[fanchors.length ? "R" : "L"].concat(fanchors).concat([this.size, 0])];
		},
		drawPath: function(){	            		
   			this.curveSet.attr({path: this.getPath() });
		},
		init: function(){			   
        	var r = this.r;
        	
        	this.drawGrid();
        	this.dragState = {
        		anchor: false,
        		leftBound: 0,
        		rightBound: this.size
        	};
        	
            var path = this.getPath();                         
            this.curve = r.path(path).attr({"stroke-width": 1, "stroke-linecap": "round"});                        
			this.invisiCurve = r.path(path).attr({"stroke-width": 60, "stroke-linecap": "round", "opacity": 0});
			this.curveSet = this.r.set(
				this.curve,
				this.invisiCurve
			);
            var node = this.r.canvas.parentNode;
          	this.invisiCurve.mouseover(function(){
          		node.style.cursor='pointer';
          	});
          	this.invisiCurve.mouseout(function(){
          		node.style.cursor='default';               		
          	});
             this.invisiCurve.drag(this.dragConfig.drag,this.dragConfig.start,this.dragConfig.end,this,this,this);
		},
		addAnchorPoint: function(x,y){
			var ap = new CurveEditor.AnchorPoint(x,y,this);
    		
    		var anchorObj = {configureDragState: this.configureDragState, anchorPoint: ap, dragState:{}};
    		ap.moveZone.drag(this.dragConfig.drag,function(x,y,e){
				this.configureDragState(x, y, e, this.anchorPoint);
    		},this.dragConfig.end,anchorObj,anchorObj,anchorObj);
    		
    		this.anchors.push(ap);
    		
    		this.anchors = this.anchors.sort(function(a,b){
    			return a.x - b.x;
    		});
    		this.drawPath();
			return ap;
		},
		drawGrid: function(){                    	
        	for(var i=0; i < this.size; i+=(this.size/10)){
				this.r.path([["M",0,i],["L",this.size,i]]).attr({"stroke-width": 1, "stroke-linecap": "round", "opacity": .5});
				this.r.path([["M",i,0],["L",i,this.size]]).attr({"stroke-width": 1, "stroke-linecap": "round", "opacity": .5});
        	}
		},
		getMappingFunction: function(debug){
			var me = this;
			
			if(debug){
				if(this.debugSet){
					this.debugSet.remove();            					
				}
				this.debugSet = this.r.set(
				    this.r.path([["M",0,0],["L",0,me.size]]).attr({"stroke-width" : 1, opacity: .75}),
   				 	this.r.circle(0, me.size, 10).attr({fill: '#0CC', opacity: .5})
				);
			}
			
			return function(x){
				var scaled = x*me.size;
				var yIntercept = Raphael.pathIntersection([["M",scaled,-100],["L",scaled,me.size+100]],me.getPath())[0].y;
				if(debug){
					me.debugSet[0].attr({path: [["M",scaled,0],["L",scaled,me.size]]});
					me.debugSet[1].attr({cx: scaled, cy: yIntercept });
				} 
				return 1 - (yIntercept/me.size);
			};
		},
		configureDragState: function(x,y,e,ap){
			this.dragState.offsetX = x-e.layerX;
			this.dragState.offsetY = y-e.layerY;
			if(ap){
				this.dragState.anchor = ap;
			} else {
				this.dragState.anchor = this.addAnchorPoint(x-this.dragState.offsetX,y-this.dragState.offsetY);				
			}
    		var bounds = this.dragState.anchor.getBounds();
    		this.dragState.leftBound = bounds[0];
    		this.dragState.rightBound = bounds[1];
			
		},
		dragConfig: {
			start: function(x,y,e){
				this.configureDragState(x,y,e);
			},
		    drag: function(dx,dy,x,y,e){
		    	var editor = this.dragState.anchor.editor;
		    	x = x - this.dragState.offsetX;
		    	y = y - this.dragState.offsetY;
		    	
          		if(!this.dragState.anchor){return false;}
                if(x < this.dragState.leftBound  ||
                   x > this.dragState.rightBound ||
                   y < 0                         ||
                   y > editor.size){                                  
                    for(var i=0; i< editor.anchors.length; i++){
                        if(editor.anchors[i]===this.dragState.anchor){
                            editor.anchors.splice(i,1); 
                            break;
                        }
                    }
                    
                    if(this.dragState.anchor){                                    	
                    	this.dragState.anchor.handleSet.hide();
                    	this.removeOnEnd = this.dragState.anchor.handleSet;
                    }                                  
                    this.dragState.anchor = false; 
                } else {                	
                    this.dragState.anchor.set(x, y);
                }
                editor.drawPath();                      		                		
        	},
		    end: function end(x,y,e){
          		if(this.dragState.anchor.handle){
          			this.dragState.anchor.handle.attr({fill: "#000"});
          		}
          		if(this.removeOnEnd){
          			this.removeOnEnd.remove();                        			
          		}
         	}
		},
		util: {
			flatten: function(array){
		        var flat = [];
		        for (var i = 0, l = array.length; i < l; i++){
		            var type = Object.prototype.toString.call(array[i]).split(' ').pop().split(']').shift().toLowerCase();
		            if (type) { flat = flat.concat(/^(array|collection|arguments|object)$/.test(type) ? arguments.callee(array[i]) : array[i]); }
		        }
		        return flat;
		    }
		}
};


CurveEditor.AnchorPoint = function(x,y,editor){
    this.x = x;
    this.y = y;
    this.editor = editor;
    this.handle = this.editor.r.rect(x-2,y-2,4,4);
    this.moveZone = this.editor.r.rect(x-2,y-2,6,6,10).attr({"stroke-width": 10, "opacity": 0});
    this.handleSet = this.editor.r.set(
    	this.handle,
    	this.moveZone
    );
	this.moveZone.mouseover(function(){
		document.getElementById('holder').style.cursor='pointer';
   	});
   	this.moveZone.mouseout(function(){
   		document.getElementById('holder').style.cursor='default';               		
   	});    
    
};
CurveEditor.AnchorPoint.prototype = {
    setX: function(val){
        this.x = val; 
    },
    setY: function(val){
        this.y = val;
    },
    set: function(x,y){
        this.setX(x);
        this.setY(y); 
        this.handleSet.attr({x: x-2,y: y-2});                    
    },
    getPoints: function(){
        return [this.x, this.y];
    },
    getBounds: function(){
		// find bounds
		var secInd = 0;
        var leftBound = 0; 
        var rightBound = this.editor.size;
        var anchors = this.editor.anchors;
        
		for(var i=0; i<anchors.length; i++){
            if(anchors[i]===this){
                secInd = i; 
                break;
            }
		}                        		

		if(secInd > 0){
            leftBound = anchors[secInd - 1].x;
		}
		if(secInd < (anchors.length-1)){
            rightBound = anchors[secInd + 1].x;
        }
    	return [leftBound, rightBound];
    }
};           

